from random import randint, choice


class BagKegs:
    """
    Класс имитирующий мешок (bag) с бочонками (kegs)

    """
    MAX_KEGS = 90

    def __init__(self):
        self.bag = []
        self.start()

    def start(self):
        self.bag = [{'value': v, 'status': 'in'} for v in range(1, self.MAX_KEGS + 1)]
        self.update_status(len(self.__get_kegs_in_bag()))

    def update_status(self, kegs_in_bag):
        self.status = {
            'kegs_amount': self.MAX_KEGS,
            'kegs_in_bag': kegs_in_bag,
            'kegs_used': self.MAX_KEGS - kegs_in_bag
        }

    def print_bag(self, print_full_info=False):
        if print_full_info:
            print(self.bag)
        print(self.status)

    def __get_kegs_in_bag(self):
        # Получаем список индексов всех бочонков которые в мешке
        return [index for index, val in enumerate(self.bag) if val['status'] == 'in']

    def get_keg_from_bag(self) -> int:
        if self.status['kegs_in_bag'] == 0:
            return -1
        kegs_in_bag = self.__get_kegs_in_bag()
        idx = choice(kegs_in_bag)
        self.bag[idx]['status'] = 'out'
        self.update_status(len(kegs_in_bag) - 1)
        return self.bag[idx]['value']

    def get_kegs_in_bag(self):
        idx_kegs_in_bag = self.__get_kegs_in_bag()
        kegs_in_bag = [str(self.bag[v]['value']) for v in idx_kegs_in_bag]
        return ', '.join(kegs_in_bag)

    @property
    def kegs_in_bag(self):
        return self.status['kegs_in_bag']

