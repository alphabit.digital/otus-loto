from random import randint
import colorama
from colorama import Fore, Style


colorama.init()

class LotoCard:
    """
    Класс карточка лото
    """
    def __init__(self, owner_name='Свободная карта'):
        self.owner_name = owner_name
        self.card = []

        # Заполняем нулями
        for row in range(0, 3):
            row_value = [{'value': 0, 'status': 'opened'} for i in range(0, 9)]
            self.card.append(row_value)

        # Заполняем в строке 5 ячеек разных десяток случайными значениями
        for row in range(0, 3):
            row_cnt = 5
            row_value = self.card[row]
            while row_cnt > 0:
                value = randint(1, 90)
                col = value // 10
                if value == 90:
                    col = 8
                # print('row_value={}, row_cnt={}, col={}, value={}'.format(row_value, row_cnt, col, value))
                if not self.is_value_exist(value) and row_value[col]['value'] == 0:
                    row_value[col]['value'] = value
                    row_value[col]['status'] = 'open'
                    row_cnt -= 1

    def is_value_exist(self, v):
        for row in self.card:
            for el in row:
                if v == el['value']:
                    return True
        return False

    def where_value_exist(self, v):
        for row_idx, row in enumerate(self.card):
            for el_idx, el in enumerate(row):
                if v == el['value']:
                    return tuple([row_idx, el_idx])
        return None

    def print_card(self):
        def draw_line(char):
            print(char*55)

        print(self.owner_name+':')
        draw_line('-')
        for row in range(0, 3):
            for col in range(0, 9):
                value = self.card[row][col]['value']
                status  = self.card[row][col]['status']
                if status == 'close':
                    print(Fore.RED + ' -{:2}- '.format(value if value else ''), end='')
                else:
                    print(Style.RESET_ALL + '  {:2}  '.format(value if value else ''), end='')
            print(Style.RESET_ALL + '')
        draw_line('=')

    def put_keg(self, keg_value):
        # Получаем координаты значения или None если значения нет
        res = self.where_value_exist(keg_value)
        if not res:
            return False
        self.card[res[0]][res[1]]['status'] = 'close'
        return True

    def is_win(self) -> bool:
        for row in range(0, 3):
            for col in range(0, 9):
                if self.card[row][col]['status'] == 'open':
                    return False
        return True
