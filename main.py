import colorama
from colorama import Fore, Style
import LotoGame as loto


def run_proc():
    card_1 = loto.LotoCard('Ваша карта')
    card_2 = loto.LotoCard('Карта Чебуратора')

    bag = loto.BagKegs()
    game_over = False
    while (bag.kegs_in_bag > 0) and not game_over:
        keg = bag.get_keg_from_bag()
        card_1.print_card()
        card_2.print_card()
        card_2.put_keg(keg)
        print(f"Бочонок {keg}. Осталось бочонков: {bag.kegs_in_bag}")
        if input('Закрываем поле(y/n) (+/-)? ') in ('y', 'Y', 'д', 'Д', '+'):
            res = card_1.put_keg(keg)
            if not res:
                game_over = not res
                print(Fore.RED + f'Такого поля со значением {keg} нет! Вы проиграли')
                break
        elif card_1.is_value_exist(keg):
            game_over = True
            print(Fore.RED + f'Вы пропустили поле со значением {keg}! Вы проиграли')
            break
        # Если закрылись все поля
        win1 = card_1.is_win()
        win2 = card_2.is_win()
        if win1 and win2:
            print(Fore.YELLOW + 'У обоих игроков закрылись карты. НИЧЬЯ!!!')
        elif win1:
            print(Fore.GREEN + 'Поздравляем!!! Вы виграли')
        elif win2:
            print(Fore.RED + 'Компьютер выиграл!!! Вы проиграли')
        game_over = win1 or win2
    print('Оставшиеся бочонки в мешке: \n', bag.get_kegs_in_bag())
    print(Style.RESET_ALL + '\nИгра закончена')


if __name__ == "__main__":
    run_proc()
